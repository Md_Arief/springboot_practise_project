package com.example.demo.helloworld.controller;

import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/helloworld")
public class helloWorldController {

    @GetMapping()
    public String getheStringllo() {
        return "Hello World!!";
    }

    @PostMapping
    public String paramExample(@RequestParam("name") String name,
                               @RequestParam("age") int age,
                               @RequestParam(value = "admin", defaultValue = "false")boolean isAdmin,
                               @RequestBody Map<String,Object> obj){
        return "Name : "+name +"\nage : "+age+"\nand admin status is : "+isAdmin+"\n"+obj.toString();
    }

    @PostMapping("/example/{name}/{id}/{isAdmin}")
    public String PathExample(@PathVariable("name") String name,
                              @PathVariable("id") int id,
                              @PathVariable("isAdmin") boolean isAdmin,
                              @RequestBody Map<String,Object> obj){
        return "Name : "+name +"\nid : "+id+"\nand admin status is : "+isAdmin+"\n"+obj.toString();
    }

}
